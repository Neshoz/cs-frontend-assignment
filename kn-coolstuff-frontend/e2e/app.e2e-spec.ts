import { KnCoolstuffFrontendPage } from './app.po';

describe('kn-coolstuff-frontend App', () => {
  let page: KnCoolstuffFrontendPage;

  beforeEach(() => {
    page = new KnCoolstuffFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
