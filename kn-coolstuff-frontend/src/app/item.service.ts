import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ItemService {
    constructor(private http: Http) {}

    getToken() {
        return this.http.post('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/', {'country_code': 'de'})
        .map((res: Response) => res.json());
    }

    addToCart(token, items) {
        return this.http.put('https://xcaxla5hr1.execute-api.eu-west-1.amazonaws.com/v1/carts/' + token, items)
        .map((res: Response) => res.json());
    }
}
