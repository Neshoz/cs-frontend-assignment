export class Item {
    constructor(public name: string, public price: Number, public image: string, public id: Number) {}
}
