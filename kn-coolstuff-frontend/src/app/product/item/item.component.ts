import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Item } from '../../item.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() itemElement: Item;

  @Output() onBuyClicked = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onBuyClick() {
    this.onBuyClicked.emit();
    console.log('Event emitted');
  }

}
