import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Item } from '../../item.model';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {
  @Output()
  passItem = new EventEmitter<Item>();

  // Holds the articles as "items".
  items: Item[] = [
    new Item('Coffee', 10, '../../assets/coffee.png', 102),
    new Item('Red Bull', 10, '../../assets/redbull.png', 88)
  ];

  constructor() { }

  ngOnInit() {
  }

  onBuyEventReceived(item: Item) {
    this.passItem.emit(item);
  }

}
