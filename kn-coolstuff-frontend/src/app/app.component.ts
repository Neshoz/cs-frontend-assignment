import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Item } from './item.model';
import { ItemService } from './item.service';
import { Article } from './article.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ItemService]
})
export class AppComponent implements OnInit {
  token = {token: '', country_code: ''};
  response = {};
  receivedItem: Item;
  itemsInCart = [];
  articles = {
    'articles': [
      {
        'article_id': Number,
        'article_quantity': Number
      }
    ],
    'language_code': 'de',
    'currency_code': 'EUR',
    'country_code': 'de'
  };

  constructor(private itemService: ItemService) {}
  ngOnInit() {
    this.getToken();
  }

  getToken() {
    this.itemService.getToken().subscribe(
      data => this.token = data,
      error => console.log(error),
      () => window.localStorage.setItem('token', this.token.token)
    );
  }

  onItemReceived(item: Item) {
    this.itemsInCart.push({'article_id': item.id, 'article_quantity': 1});
    this.articles.articles = this.itemsInCart;

    this.itemService.addToCart(window.localStorage.getItem('token'), this.articles).subscribe(
      data => this.response = data,
      error => console.log(error),
      () => console.log(this.response)
    );
    console.log(this.token.token);
  }
}
